﻿using UnityEngine;
using System.Collections;

public class Destroyable : MonoBehaviour
{
    public int hp = 1;
    public static Color[] damageColors = { Color.red, Color.blue, Color.green };
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Damage();
    }

    void Damage()
    {
        hp -= 1;
		var animator = GetComponent<Animator> ();
        if (hp <= 0) {
			animator.Play ("Die");
			GetComponent<CircleCollider2D>().enabled = false;
			Die ();
		} else {
			animator.Play("Hit");
        }
    }

    void Die()
    {
		gameObject.tag = "Untagged";
		(GameObject.Find ("/Level").GetComponent<Level>()).OnBlockDestroyed (this);
        Destroy(gameObject, 1f);
    }
}

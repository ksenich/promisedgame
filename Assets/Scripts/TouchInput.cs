﻿using UnityEngine;
using System.Collections;

public class TouchInput : MonoBehaviour {
	const float FieldDepth = 9f;
	public DrawingRegion region;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var touch = Input.GetTouch (0);
		var tp = touch.position;
		switch(touch.phase){
		case TouchPhase.Began:
			region.StartTrail (ProjectTouch (tp));
			break;
		case TouchPhase.Moved:
			region.AddToTrail (ProjectTouch (tp));	
			break;
		case TouchPhase.Ended:
		case TouchPhase.Canceled:
			region.EndTrail (ProjectTouch (tp));	
			break;
		}
	}

	Vector2 ProjectTouch (Vector3 mp)
	{
		mp.z = FieldDepth;
		var wp = Camera.main.ScreenToWorldPoint (mp);
		return new Vector2 (wp.x, wp.y);
	}
}

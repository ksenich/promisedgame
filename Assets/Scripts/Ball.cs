﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	Rigidbody2D body;
	public ParticleSystem sparks;
	public float velocity;
	public Vector2 v;
	// Use this for initialization
	void Start () 
    {
		body = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () 
    {
		body.velocity = body.velocity.normalized * velocity;
		v = body.velocity;
	}

	public void Launch(){
		body.velocity = new Vector2 (velocity, velocity);
	}

	void OnCollisionEnter2D (Collision2D collision)
    {
        if (true)//CollidingWithDestroyable(collision))
        {
            var p = collision.contacts[0].point;
            CreateSparks(p);
        }
	}

    void CreateSparks(Vector2 point)
    {
        var s = (ParticleSystem) Instantiate(sparks, 
            new Vector3(point.x, point.y, 0), 
            Quaternion.identity);
        Destroy(s, 0.1f);
    }

    private bool CollidingWithDestroyable(Collision2D collision)
    {
        return collision.contacts[0].otherCollider.GetComponent<Destroyable>() != null;
    }
	
}

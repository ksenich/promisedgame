﻿using UnityEngine;
using System.Collections;

public class MouseInput : MonoBehaviour
{
	const int FieldDepth = 9;
	public DrawingRegion region;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnMouseDown ()
	{	
		var mp = Input.mousePosition;
		region.StartTrail (ProjectMouse (mp));
	}

	void OnMouseDrag ()
	{
		
		var mp = Input.mousePosition;
		region.AddToTrail (ProjectMouse (mp));	
	}
	
	void OnMouseUp ()
	{
		var mp = Input.mousePosition;
		region.EndTrail (ProjectMouse (mp));	
	}
	
	Vector2 ProjectMouse (Vector3 mp)
	{
		mp.z = FieldDepth;
		var wp = Camera.main.ScreenToWorldPoint (mp);
		return new Vector2 (wp.x, wp.y);
	}

}

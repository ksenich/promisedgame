﻿using UnityEngine;
using System.Collections;

public class EndLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Next(){
		Application.LoadLevel (Application.loadedLevel + 1);
	}

	public void Retry(){
		Application.LoadLevel (Application.loadedLevel);
	}

	public void Quit(){
		Application.LoadLevel ("LevelSelect");
	}
}

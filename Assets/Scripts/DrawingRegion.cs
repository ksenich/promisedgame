﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawingRegion : MonoBehaviour
{

	public EdgeCollider2D trailCollider;
	public LineRenderer trailRenderer;
	public float minimalDistance = 0.1f;

	BoxCollider2D boxCollider;
	List<Vector2> path = new List<Vector2> ();

	// Use this for initialization
	void Start ()
	{
		boxCollider = GetComponent<BoxCollider2D> ();
	}

	// Update is called once per frame
	void Update ()
	{
		UpdateRenderer ();
	}

	void UpdateRenderer ()
	{
		trailRenderer.SetVertexCount (trailCollider.points.Length);
		for (int i = 0; i < trailCollider.points.Length; i++) {
			var p = trailCollider.points [i];
			trailRenderer.SetPosition (i, new Vector3 (p.x, p.y, -2));
		}
	}
	
	public void StartTrail (Vector2 worldPos)
	{
		path.Clear ();
		AddToTrail (worldPos);
	}

	public void AddToTrail (Vector2 point)
	{
		if (path.Count == 0) {
			path.Add (point);
			path.Add (point + new Vector2 (.1f, .1f));
		} else {
			var last = trailCollider.points [trailCollider.points.Length - 2];
			if (
				(last - point).magnitude > minimalDistance &&
				boxCollider.OverlapPoint(point)
			) {
				path.Add (point);
			} else {
				trailCollider.points [trailCollider.points.Length - 1] = point;
			}
		}
        
		trailCollider.points = path.ToArray ();

	}

	public void EndTrail (Vector2 pos)
	{
		AddToTrail (pos); 
	}

}

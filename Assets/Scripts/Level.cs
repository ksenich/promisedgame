﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Level : MonoBehaviour {

	private const string winMessage = "You have finished this level.";
	private const string loseMessage = "What a shame.";

	public Text message;
	public GameObject messagePanel;
	// Use this for initialization
	void Start () {
		messagePanel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnBlockDestroyed(Destroyable d){
		Debug.Log (GameObject.FindGameObjectsWithTag ("Destroy").Length);
		if (GameObject.FindGameObjectsWithTag ("Destroy").Length == 0) {
			Win ();
		}
	}

	public void Win(){
		message.text = winMessage;
		messagePanel.SetActive(true);
	}

	public void Lose(){
		message.text = loseMessage;
		messagePanel.SetActive(true);
	}
}
